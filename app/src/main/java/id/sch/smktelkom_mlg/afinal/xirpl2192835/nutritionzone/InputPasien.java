package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model.DataPasien;

public class InputPasien extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference databasePasien;
    private EditText eNama, eT, eTL, eUmur, eAlamat;
    private Spinner eGender;
    private Button bInput;
    private Toolbar mToolbar;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = mAuth.getCurrentUser();
    private String userId = user.getUid();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_pasien);

        mToolbar = findViewById(R.id.toolbarinput);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Tambahkan Pasien");

        databasePasien = database.getReference(userId).child("pasien");
        eNama = findViewById(R.id.nama);
        eT = findViewById(R.id.tempatlahir);
        eTL = findViewById(R.id.tgllahir);
        eGender = findViewById(R.id.gender);
        eUmur = findViewById(R.id.umur);
        eAlamat = findViewById(R.id.alamat);
        bInput = findViewById(R.id.inputPasien);

        bInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });

    }

    private void addData() {
        String nama = eNama.getText().toString().trim();
        String t = eT.getText().toString().trim();
        String tl = eTL.getText().toString().trim();
        String ttl = t + ", " + tl;
        String gender = eGender.getSelectedItem().toString().trim();
        String umur = eUmur.getText().toString().trim();
        String alamat = eAlamat.getText().toString().trim();

        if (TextUtils.isEmpty(nama)) {
            Toast.makeText(this, "Tolong isi Nama", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(ttl)) {
            Toast.makeText(this, "Tolong isi Tempat, Tanggal Lahir", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(gender)) {
            Toast.makeText(this, "Tolong isi Jenis Kelamin", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(umur)) {
            Toast.makeText(this, "Tolong isi Umur", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(alamat)) {
            Toast.makeText(this, "Tolong isi Alamat", Toast.LENGTH_LONG).show();
        } else {
            String id = databasePasien.push().getKey();
            DataPasien data = new DataPasien(nama, ttl, gender, umur, alamat);
            databasePasien.child(id).setValue(data);
            startActivity(new Intent(InputPasien.this, Tablayout.class));
            finish();
            Toast.makeText(this, "Data Added", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
