package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.Map;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.DetailKondisi;
import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model.DataPasien;
import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class KondisiFragment extends Fragment {


    public KondisiFragment() {
        // Required empty public constructor
    }


    private Query query;
    private FirebaseRecyclerAdapter<DataPasien, PasienViewHolder> firebaseRecyclerAdapter;
    private ProgressDialog progressDialog;
    private DatabaseReference databasePasien;
    private RecyclerView recyclerView;
    private String nama, penyakit, status;
    private SearchView searchView;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = mAuth.getCurrentUser();
    private String userId = user.getUid();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem mSearchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String text) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    query = databasePasien;
                    retrieveData(query);
                    firebaseRecyclerAdapter.startListening();
                } else {
                    query = databasePasien.orderByChild("nama").startAt(newText).endAt("\uf8ff");
                    retrieveData(query);
                    firebaseRecyclerAdapter.startListening();
                }
                return true;
            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kondisi, container, false);
        progressDialog = new ProgressDialog(getActivity());
        databasePasien = FirebaseDatabase.getInstance().getReference(userId).child("pasien");
        databasePasien.keepSynced(true);
        recyclerView = view.findViewById(R.id.recyclerView);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //recyclerView.setLayoutManager(layoutManager);
        //start getdb
        query = databasePasien;
        retrieveData(query);
        // end db


        return view;

    }

    private void retrieveData(Query query) {
        FirebaseRecyclerOptions<DataPasien> options =
                new FirebaseRecyclerOptions.Builder<DataPasien>()
                        .setQuery(query, DataPasien.class)
                        .build();
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<DataPasien, PasienViewHolder>(options) {
            @Override
            protected void onBindViewHolder(PasienViewHolder pasienViewHolder, int position, final DataPasien dataPasien) {
                final String pasienID = getRef(position).getKey();

                final Map<String, String> arraycheck = dataPasien.getCheck();

                Date date = new Date();
                String day = (String) DateFormat.format("dd", date);

                progressDialog.dismiss();
                nama = dataPasien.getNama();
                if (arraycheck != null) {
                    status = "-" + arraycheck.get("aktifitas");
                    penyakit = "-" + arraycheck.get("stres");

                    pasienViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getActivity(), DetailKondisi.class);
                            //Toast.makeText(MainActivity.this,wisataID,Toast.LENGTH_SHORT).show();
                            i.putExtra("id", pasienID);
                            i.putExtra("nama", dataPasien.getNama());
                            i.putExtra("status", arraycheck.get("status"));
                            i.putExtra("bb", arraycheck.get("bb"));
                            i.putExtra("tb", arraycheck.get("tb"));
                            i.putExtra("tee", arraycheck.get("tee"));
                            i.putExtra("aktifitas", arraycheck.get("aktifitas"));
                            i.putExtra("stress", arraycheck.get("stres"));
                            i.putExtra("imt", arraycheck.get("imt"));
                            if (arraycheck.get("tEaten") != null) {
                                i.putExtra("tEaten", arraycheck.get("tEaten"));
                            } else {
                                i.putExtra("tEaten", "0");
                            }
                            startActivity(i);
                        }
                    });
                } else {
                    status = "";
                    penyakit = "";
                    pasienViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getActivity(), "Tolong Cek Kondisi Pasien Dahulu", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                Log.d("Value", "Value Check: " + arraycheck);
                String tEaten;
                if (arraycheck != null) {
                    if (arraycheck.get("tEaten") != null) {
                        tEaten = arraycheck.get("tEaten");
                        Double d = new Double(Double.parseDouble(arraycheck.get("tee")));
                        int intd = d.intValue();
                        int intt = Integer.parseInt(tEaten);
                        if (intt == intd) {
                            pasienViewHolder.setBg();
                        } else {
                            pasienViewHolder.setBgWhite();
                        }
                    } else {
                        tEaten = "0";
                    }
                } else {
                }

                pasienViewHolder.setNama(nama);
                pasienViewHolder.setTtl(penyakit);
                pasienViewHolder.setUmur(status);

            }

            @Override
            public PasienViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_list2, parent, false);
                return new PasienViewHolder(view);
            }

        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);

    }

    @Override
    public void onStart() {
        super.onStart();
        progressDialog.setMessage("Load Data....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        checkData();
        firebaseRecyclerAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (firebaseRecyclerAdapter != null) {
            firebaseRecyclerAdapter.stopListening();
        }
    }

    protected static class PasienViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public PasienViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setBg() {
            CardView cvK = mView.findViewById(R.id.cardviewk);
            cvK.setBackgroundResource(R.color.greenDone);
        }

        public void setBgWhite() {
            CardView cvK = mView.findViewById(R.id.cardviewk);
            cvK.setBackgroundResource(R.color.white);

        }

        public void setNama(String nama) {
            TextView txtNama = mView.findViewById(R.id.tvNama);
            txtNama.setText(nama);
        }

        public void setTtl(String ttl) {
            TextView txtTtl = mView.findViewById(R.id.tvTTL);
            txtTtl.setText(ttl);
        }

        public void setUmur(String umur) {
            TextView txtUmur = mView.findViewById(R.id.tvUmur);
            txtUmur.setText(umur);
        }
    }

    private void checkData() {
        databasePasien.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                DataPasien value = dataSnapshot.getValue(DataPasien.class);
                Log.d("valuepasiendatasnap", "Value is: " + value);
                if (value == null) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Data Kosong. Tambahkan Pasien", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("valuepasiendatasnap", "Failed to read value.", error.toException());
            }
        });
    }
}
