package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.Map;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.DetailGizi;
import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model.DataPasien;
import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GiziFragment extends Fragment {
    private Query query;
    private FirebaseRecyclerAdapter<DataPasien, GiziViewHolder> firebaseRecyclerAdapter;
    private ProgressDialog progressDialog;
    private DatabaseReference databasePasien;
    private RecyclerView recyclerView;
    private String nama, tee;
    private int itee;
    private SearchView searchView;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = mAuth.getCurrentUser();
    private String userId = user.getUid();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem mSearchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String text) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    query = databasePasien;
                    retrieveData(query);
                    firebaseRecyclerAdapter.startListening();
                } else {
                    query = databasePasien.orderByChild("nama").startAt(newText).endAt("\uf8ff");
                    retrieveData(query);
                    firebaseRecyclerAdapter.startListening();
                }
                return true;
            }
        });
    }
    public GiziFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gizi, container, false);

        progressDialog = new ProgressDialog(getActivity());
        databasePasien = FirebaseDatabase.getInstance().getReference(userId).child("pasien");
        databasePasien.keepSynced(true);
        recyclerView = view.findViewById(R.id.recyclerView);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //recyclerView.setLayoutManager(layoutManager);
        //start getdb
        query = databasePasien;
        retrieveData(query);
        // end db


        return view;

    }

    private void retrieveData(Query query) {
        FirebaseRecyclerOptions<DataPasien> options =
                new FirebaseRecyclerOptions.Builder<DataPasien>()
                        .setQuery(query, DataPasien.class)
                        .build();
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<DataPasien, GiziViewHolder>(options) {
            @Override
            protected void onBindViewHolder(GiziViewHolder giziViewHolder, int position, final DataPasien dataPasien) {
                final String pasienID = getRef(position).getKey();

                final Map<String, String> arraycheck = dataPasien.getCheck();

                Date date = new Date();
                String day = (String) DateFormat.format("dd", date);

                progressDialog.dismiss();
                nama = dataPasien.getNama();
                giziViewHolder.setNama(nama);
                if (arraycheck != null) {
                    tee = arraycheck.get("tee");
                    Double dtee = new Double(Double.parseDouble(tee));
                    itee = dtee.intValue();
                    int makan3x = itee / 3;
                    giziViewHolder.setPagiSiangMalam(makan3x);
                    giziViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getActivity(), DetailGizi.class);
                            //Toast.makeText(MainActivity.this,wisataID,Toast.LENGTH_SHORT).show();
                            i.putExtra("id", pasienID);
                            i.putExtra("nama", dataPasien.getNama());
                            i.putExtra("tee", arraycheck.get("tee"));
                            startActivity(i);
                        }
                    });
                } else {
                    giziViewHolder.setPagiSiangMalam(000000);
                    giziViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getActivity(), "Tolong Cek Kondisi Pasien Dahulu", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                Log.d("Value", "Value Check: " + arraycheck);

            }

            @Override
            public GiziViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_list3, parent, false);
                return new GiziViewHolder(view);
            }

        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);

    }

    @Override
    public void onStart() {
        super.onStart();
        progressDialog.setMessage("Load Data....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        checkData();
        firebaseRecyclerAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (firebaseRecyclerAdapter != null) {
            firebaseRecyclerAdapter.stopListening();
        }
    }

    protected static class GiziViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public GiziViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setNama(String nama) {
            TextView txtNama = mView.findViewById(R.id.tvNama);
            txtNama.setText(nama);
        }

        public void setPagiSiangMalam(int makan3x) {
            TextView txtPagi = mView.findViewById(R.id.tvMorning);
            TextView txtSiang = mView.findViewById(R.id.tvNoon);
            TextView txtMalam = mView.findViewById(R.id.tvEvening);
            txtPagi.setText("Pagi : " + makan3x + " kkal");
            txtSiang.setText("Siang : " + makan3x + " kkal");
            txtMalam.setText("Malam : " + makan3x + " kkal");
        }

    }

    private void checkData() {
        databasePasien.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                DataPasien value = dataSnapshot.getValue(DataPasien.class);
                Log.d("valuepasiendatasnap", "Value is: " + value);
                if (value == null) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Data Kosong. Tambahkan Pasien", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("valuepasiendatasnap", "Failed to read value.", error.toException());
            }
        });
    }
}
