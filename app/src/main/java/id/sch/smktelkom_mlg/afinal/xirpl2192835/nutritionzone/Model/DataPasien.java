package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model;

import java.util.Map;

public class DataPasien {
    String nama;
    String ttl;
    String gender;
    String umur;
    String alamat;
    String tb;
    String bb;
    String aktifitas;
    String stres;
    String tee;
    String status;
    String imt;
    String tEaten;
    Map<String, String> data;
    Map<String, String> check;


    public DataPasien() {
    }

    public DataPasien(String tb, String bb, String aktifitas, String stres, String tee, String status, String imt, String tEaten) {
        this.tb = tb;
        this.bb = bb;
        this.aktifitas = aktifitas;
        this.stres = stres;
        this.tee = tee;
        this.status = status;
        this.imt = imt;
        this.tEaten = tEaten;
    }

    public DataPasien(String nama, String ttl, String gender, String umur, String alamat, String tb, String bb, String aktifitas, String stres, String tee, String status) {
        this.nama = nama;
        this.ttl = ttl;
        this.gender = gender;
        this.umur = umur;
        this.alamat = alamat;
        this.tb = tb;
        this.bb = bb;
        this.aktifitas = aktifitas;
        this.stres = stres;
        this.tee = tee;
        this.status = status;
    }

    public DataPasien(String nama, String ttl, String gender, String umur, String alamat) {
        this.nama = nama;
        this.ttl = ttl;
        this.gender = gender;
        this.umur = umur;
        this.alamat = alamat;
    }

    public DataPasien(String tb, String bb, String aktifitas, String stres, String tee, String status, String imt) {
        this.tb = tb;
        this.bb = bb;
        this.aktifitas = aktifitas;
        this.stres = stres;
        this.tee = tee;
        this.status = status;
        this.imt = imt;
    }


    public String gettEaten() {
        return tEaten;
    }

    public Map<String, String> getData() {
        return data;
    }

    public Map<String, String> getCheck() {
        return check;
    }

    public String getNama() {
        return nama;
    }

    public String getTtl() {
        return ttl;
    }

    public String getGender() {
        return gender;
    }

    public String getUmur() {
        return umur;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getTb() {
        return tb;
    }

    public String getBb() {
        return bb;
    }

    public String getAktifitas() {
        return aktifitas;
    }

    public String getStres() {
        return stres;
    }

    public String getTee() {
        return tee;
    }

    public String getStatus() {
        return status;
    }

    public String getImt() {
        return imt;
    }
}
