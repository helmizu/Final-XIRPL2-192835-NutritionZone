package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model.DataPasien;

public class Pasien extends AppCompatActivity {
    private Query query;
    private FirebaseRecyclerAdapter<DataPasien, PasienViewHolder> firebaseRecyclerAdapter;
    private ProgressDialog progressDialog;
    private DatabaseReference databasePasien;
    private RecyclerView recyclerView;
    private String nama, ttl, umur, gender, alamat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasien);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        databasePasien = FirebaseDatabase.getInstance().getReference();
        query = databasePasien.child("pasien");
        recyclerView = findViewById(R.id.recyclerView);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //recyclerView.setLayoutManager(layoutManager);

        databasePasien.keepSynced(true);
        progressDialog = new ProgressDialog(this);
        //start getdb
        FirebaseRecyclerOptions<DataPasien> options =
                new FirebaseRecyclerOptions.Builder<DataPasien>()
                        .setQuery(query, DataPasien.class)
                        .build();
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<DataPasien, PasienViewHolder>(options) {
            @Override
            protected void onBindViewHolder(PasienViewHolder pasienViewHolder, int position, DataPasien dataPasien) {
                final String pasienID = getRef(position).getKey();

                progressDialog.dismiss();
                nama = dataPasien.getNama();
                ttl = dataPasien.getTtl();
                gender = dataPasien.getGender();
                umur = dataPasien.getUmur();
                alamat = dataPasien.getAlamat();

                pasienViewHolder.setNama(nama);
                pasienViewHolder.setTtl(ttl);
                pasienViewHolder.setGender(gender);
                pasienViewHolder.setUmur(umur);
                pasienViewHolder.setAlamat(alamat);

                pasienViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Pasien.this, DetailActivity.class);
                        //Toast.makeText(MainActivity.this,wisataID,Toast.LENGTH_SHORT).show();
                        i.putExtra("id", pasienID);
                        i.putExtra("nama", nama);
                        startActivity(i);
                    }
                });

            }

            @Override
            public PasienViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_list, parent, false);
                return new PasienViewHolder(view);
            }

        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
        // end db

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Pasien.this, InputPasien.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        progressDialog.setMessage("Load Data....");
        progressDialog.show();
        firebaseRecyclerAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (firebaseRecyclerAdapter != null) {
            firebaseRecyclerAdapter.stopListening();
        }
    }

    protected static class PasienViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public PasienViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setNama(String nama) {
            TextView txtNama = mView.findViewById(R.id.tvNama);
            txtNama.setText(nama);
        }

        public void setTtl(String ttl) {
            TextView txtTtl = mView.findViewById(R.id.tvTTL);
            txtTtl.setText(ttl);
        }

        public void setGender(String gender) {
            TextView txtGender = mView.findViewById(R.id.tvGender);
            txtGender.setText(gender);
        }

        public void setUmur(String umur) {
            TextView txtUmur = mView.findViewById(R.id.tvUmur);
            txtUmur.setText(umur + " Tahun");
        }

        public void setAlamat(String alamat) {
            TextView txtAlamat = mView.findViewById(R.id.tvAlamat);
            txtAlamat.setText(alamat);
        }
    }
}
