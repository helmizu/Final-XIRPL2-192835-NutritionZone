package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model.DataPasien;

public class DetailActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference databasePasien;
    private String id, nama, umur, gender, tb, bb, aktifitas, stres, status, textTEE;
    private Double vTEE, vBEE, vFStres, vBMR, vIMT;
    private TextView tvNama, tvUmur, tvGender;
    private Spinner sAktifitas, sStres;
    private EditText eTB, eBB;
    private Button bInput, bDelete;
    private Toolbar toolbar;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = mAuth.getCurrentUser();
    private String userId = user.getUid();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Cek Kondisi Pasien");

        tvNama = findViewById(R.id.dtName);
        tvUmur = findViewById(R.id.dtUmur);
        tvGender = findViewById(R.id.dtGender);
        sAktifitas = findViewById(R.id.dtAktifitas);
        sStres = findViewById(R.id.dtStres);
        eTB = findViewById(R.id.dtTB);
        eBB = findViewById(R.id.dtBB);
        bInput = findViewById(R.id.dtSubmit);
        bDelete = findViewById(R.id.dtDelete);
        databasePasien = database.getReference(userId).child("pasien");

        id = getIntent().getStringExtra("id");
        nama = getIntent().getStringExtra("nama");
        umur = getIntent().getStringExtra("umur");
        gender = getIntent().getStringExtra("gender");
        Log.d("ValueGenderUser", "this gender : " + gender);

        tvNama.setText(nama);
        tvUmur.setText(umur + " Tahun");
        tvGender.setText(gender);

        bInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCheck();
            }
        });
        bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                final Query deleteQuery = ref.child(userId).child("pasien").child(id);

                deleteQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot deleteSnapshot : dataSnapshot.getChildren()) {
                            deleteSnapshot.getRef().removeValue();
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("DeleteData", "onCancelled", databaseError.toException());
                    }
                });
            }
        });

    }

    private void addCheck() {
        tb = eTB.getText().toString().trim();
        bb = eBB.getText().toString().trim();
        aktifitas = sAktifitas.getSelectedItem().toString().trim();
        stres = sStres.getSelectedItem().toString().trim();

        if (TextUtils.isEmpty(bb)) {
            eBB.setError("Email harus diisi");
        } else if (TextUtils.isEmpty(tb)) {
            eTB.setError("Tinggi Badan harus diisi");
        } else if (TextUtils.isEmpty(aktifitas)) {
            Toast.makeText(this, "Pilih Aktifitas", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(stres)) {
            Toast.makeText(this, "Pilih Faktor Stres", Toast.LENGTH_LONG).show();
        } else {
            vIMT = Double.parseDouble(bb) / Math.pow(Double.parseDouble(tb) / 100, 2);
            Log.d("ValueIMT", "IMT = " + String.valueOf(vIMT));

            if (vIMT < 18.50) {
                status = "Underweight";
            } else if (vIMT >= 18.50 && vIMT <= 24.99) {
                status = "Normal";
            } else if (vIMT >= 25.00 && vIMT <= 29.99) {
                status = "Pre Obese";
            } else if (vIMT >= 30.00 && vIMT <= 34.99) {
                status = "Obese Class I";
            } else if (vIMT >= 35.00 && vIMT <= 39.99) {
                status = "Obese Class II";
            } else if (vIMT > 40.00) {
                status = "Obese Class III";
            }

            Log.d("ValueIMT", "Status = " + status);
            if (gender.equals("Laki-Laki")) {
                vBEE = 66 + (Double.parseDouble(bb) * 13.7) + (5 * Double.parseDouble(tb)) - (6.8 * Double.parseDouble(umur));
                String textBEE = "66 + (Double.parseDouble(bb) * 13.7) + (5 * Double.parseDouble(tb)) - (6.8 * Double.parseDouble(umur))";
                Log.d("ValueGender", "Laki-Laki : " + textBEE);
            } else if (gender.equals("Perempuan")) {
                vBEE = 655 + (Double.parseDouble(bb) * 9.6) + (1.7 * Double.parseDouble(tb)) - (4.7 * Double.parseDouble(umur));
                String textBEE = "655 + (Double.parseDouble(bb) * 9.6) + (1.7 * Double.parseDouble(tb)) - (4.7 * Double.parseDouble(umur))";
                Log.d("ValueGender", "Perempuan : " + textBEE);
            }
            if (aktifitas.equals("Istirahat Total - 10%")) {
                vBMR = 1.1;
            } else if (aktifitas.equals("Aktifitas Ringan Sekali - 30%")) {
                vBMR = 1.3;
            } else if (aktifitas.equals("Aktifitas Ringan - 50%")) {
                vBMR = 1.5;
            } else if (aktifitas.equals("Aktifitas Sedang - 75%")) {
                vBMR = 1.75;
            } else if (aktifitas.equals("Aktifitas Berat - 100%")) {
                vBMR = 2.0;
            } else if (aktifitas.equals("Rawat Inap Rumah Sakit")) {
                vBMR = 1.3;
            }
            if (stres.equals("Tidak Sakit")) {
                vFStres = 1.0;
            } else if (stres.equals("Malnutrisi")) {
                vFStres = 1.7;
            } else if (stres.equals("Penyakit Ringan")) {
                vFStres = 1.0;
            } else if (stres.equals("Penyakit Sedang")) {
                vFStres = 1.3;
            } else if (stres.equals("Penyakit Berat")) {
                vFStres = 1.5;
            } else if (stres.equals("Rawat Inap Rumah Sakit")) {
                vFStres = 1.4;
            }
            Log.d("ValueVAktifitas", "aktifitas = " + aktifitas);
            Log.d("ValueVAktifitas", "BMR = " + vBMR);
            Log.d("ValueVStress", "stress = " + stres);
            Log.d("ValueVStress", "vStress = " + vFStres);
            Log.d("ValueVBEE", "vBEE = " + vBEE);
            Log.d("ValueVTB", "vTB = " + tb);
            Log.d("ValueVBB", "vBB = " + bb);
            vTEE = vBEE * vBMR * vFStres;
            Log.d("ValueVTEE", "vTEE = " + vTEE);
            textTEE = String.valueOf(vTEE);
            DataPasien data = new DataPasien(tb, bb, aktifitas, stres, textTEE, status, String.valueOf(vIMT));
            databasePasien.child(id).child("check").setValue(data);
            startActivity(new Intent(DetailActivity.this, Tablayout.class));
            finish();
            Toast.makeText(this, "Pasien Checked", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
