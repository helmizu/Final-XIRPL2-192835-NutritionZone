package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model.DataUser;

public class Signup extends AppCompatActivity {
    ProgressDialog progressDialog;
    EditText suNama, suPhone, suEmail, suPwd;
    Button suDaftar;
    Button suLogin;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseUser;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        suNama = findViewById(R.id.suNama);
        suPhone = findViewById(R.id.suPhone);
        suEmail = findViewById(R.id.suEmail);
        suPwd = findViewById(R.id.suPassword);
        suDaftar = findViewById(R.id.suDaftar);
        suLogin = findViewById(R.id.suLogin);
        progressDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            suNama.setText(user.getDisplayName());
            suEmail.setText(user.getEmail());
            suPhone.setText(user.getPhoneNumber());
            suDaftar.setText("Set Account");
        }
        suDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daftar();
            }
        });
        suLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Signup.this, Login.class));
                finish();
            }
        });
    }

    private void daftar() {
        final String nama, telp, email, password;
        nama = suNama.getText().toString().trim();
        telp = suPhone.getText().toString().trim();
        email = suEmail.getText().toString().trim();
        password = suPwd.getText().toString().trim();

        if (TextUtils.isEmpty(nama)) {
            suNama.setError("Nama harus diisi");
        } else if (TextUtils.isEmpty(telp)) {
            suPhone.setError("Nomor Telepon harus diisi");
        } else if (TextUtils.isEmpty(email)) {
            suEmail.setError("Email harus diisi");
        } else if (TextUtils.isEmpty(password)) {
            suPwd.setError("Password harus diisi");
        } else if (password.length() < 6) {
            suPwd.setError("Password harus lebih dari 6 digit");
        } else {
            progressDialog.setMessage("Signup....");
            progressDialog.show();
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                databaseUser = database.getReference("user");
                                String uid = mAuth.getUid();
                                DataUser data = new DataUser(nama, telp, email, password);
                                databaseUser.child(uid).setValue(data);

                                Toast.makeText(Signup.this, "Daftar Berhasil",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                // If sign in fails, display a message to the user.
                                Toast.makeText(Signup.this, "Daftar Gagal",
                                        Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();
                        }
                    });
            if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                    Toast.makeText(Signup.this, "Login Success", Toast.LENGTH_SHORT).show();
                                    finish();
                                    Log.d("provider", "providerID = " + user.getProviderId());
                                    Log.d("provider", "providerData = " + user.getProviderData());
                                    Log.d("provider", "providerS = " + user.getProviders());
                                    List<String> stringList = user.getProviders();
                                    if (stringList.contains("password")) {
                                        Log.d("provider", "Password Available");
                                    }
                                    Log.d("provider", "Total = " + stringList.size());
                                    startActivity(new Intent(Signup.this, Tablayout.class));
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Toast.makeText(Signup.this, "Login Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            } else {
                startActivity(new Intent(Signup.this, Tablayout.class));
            }
        }
    }
}


