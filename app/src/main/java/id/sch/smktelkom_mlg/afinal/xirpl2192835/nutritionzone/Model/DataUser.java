package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model;

public class DataUser {
    String nama, telp, email, password;

    public DataUser() {
    }

    public DataUser(String nama, String telp, String email, String password) {
        this.nama = nama;
        this.telp = telp;
        this.email = email;
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public String getTelp() {
        return telp;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
