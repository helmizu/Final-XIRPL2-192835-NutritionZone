package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.R;

public class WalkthroughAdapter extends PagerAdapter {
    public Integer[] imageSlide = {R.drawable.pasiensehaticon, R.drawable.nutrisiicon, R.drawable.hidupsehaticon};
    public String[] slideTextSatu = {"SELAMAT DATANG ", "FITUR SEDERHANA", "AYO HIDUP SEHAT "};
    public String[] slideTextDua = {
            "Nutrition Zone",
            "Sehat Itu Mahal",
            "Mencegah Lebih Baik Daripada Mengobati "};
    public String[] slideTextTiga = {
            "Aplikasi ini dibuat untuk membantu Anda dalam mengetahui kadar kebutuhan energi dalam tubuh Pasien Anda",
            "Dengan menggunakan aplikasi ini Anda dapat menghitung kebutuhan energi harian Pasien Anda, mengetahui status tubuh ideal Pasien Anda , dan memberikan rekomendasi total energi setiap makan",
            "Semoga aplikasi ini bisa berguna serta bermanfaat bagi kita bersama, selamat mencoba"};

    Context context;
    LayoutInflater layoutInflater;

    public WalkthroughAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return slideTextSatu.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slider_layout, container, false);
        ImageView ivSlide = view.findViewById(R.id.ivSlide);
        TextView tvSatu = view.findViewById(R.id.txtSatu);
        TextView tvDua = view.findViewById(R.id.txtDua);
        TextView tvTiga = view.findViewById(R.id.txtTiga);
        ivSlide.setImageResource(imageSlide[position]);
        tvSatu.setText(slideTextSatu[position]);
        tvDua.setText(slideTextDua[position]);
        tvTiga.setText(slideTextTiga[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
