package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SetPassword extends AppCompatActivity {
    EditText email, password;
    Button btn;
    String e, p;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        email = findViewById(R.id.editTexte);
        password = findViewById(R.id.editTextp);
        btn = findViewById(R.id.buttonsss);
        email.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPassword();
            }
        });
    }

    public void setPassword() {
        final String newPassword = password.getText().toString().trim();

        user.updatePassword(newPassword)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("setPassword", "User password updated." + newPassword);
                            startActivity(new Intent(getApplicationContext(), Tablayout.class));
                        }
                    }
                });
    }
}
