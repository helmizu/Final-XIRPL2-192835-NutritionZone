package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class Login extends AppCompatActivity {
    private static final int RC_SIGN_IN = 123;
    private static final String TAG = "error firebase : ";
    EditText e_email, e_password;
    Button btn_login;
    Button logDaftar;
    SignInButton logGoogle;
    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        e_email = findViewById(R.id.email);
        e_password = findViewById(R.id.password);
        btn_login = findViewById(R.id.login);
        logDaftar = findViewById(R.id.logDaftar);
        logGoogle = findViewById(R.id.googleSignIn);
        progressDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prosesLogin();
            }
        });
        logDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Signup.class));
                finish();
            }
        });
        logGoogle.setSize(SignInButton.SIZE_STANDARD);
        logGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googlelogin();
            }
        });
    }

    private void prosesLogin() {
        String email, password;
        email = e_email.getText().toString().trim();
        password = e_password.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            e_email.setError("Email harus diisi");
        } else if (TextUtils.isEmpty(password)) {
            e_password.setError("Password harus diisi");
        } else {
            progressDialog.setMessage("Login....");
            progressDialog.show();
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                Toast.makeText(Login.this, "Login Success", Toast.LENGTH_SHORT).show();
                                finish();
                                Log.d("provider", "providerID = " + user.getProviderId());
                                Log.d("provider", "providerData = " + user.getProviderData());
                                Log.d("provider", "providerS = " + user.getProviders());
                                List<String> stringList = user.getProviders();
                                if (stringList.contains("password")) {
                                    Log.d("provider", "Password Available");
                                }
                                Log.d("provider", "Total = " + stringList.size());
                                startActivity(new Intent(Login.this, Tablayout.class));
                            } else {
                                // If sign in fails, display a message to the user.
                                Toast.makeText(Login.this, "Login Failed", Toast.LENGTH_SHORT).show();
                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                            }
                            progressDialog.dismiss();
                        }
                    });
        }
    }

    private void googlelogin() {
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.GoogleBuilder().build());
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse repsonse = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                Log.d("provider", "providerID = " + user.getProviderId());
                Log.d("provider", "providerData = " + user.getProviderData());
                Log.d("provider", "providerS = " + user.getProviders());
                List<String> stringList = user.getProviders();
                if (stringList.contains("password")) {
                    Log.d("provider", "Password Available");
                    Intent intent = new Intent(this, Tablayout.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, SetPassword.class);
                    intent.putExtra("name", user.getDisplayName());
                    intent.putExtra("email", user.getEmail());
                    intent.putExtra("phone", user.getPhoneNumber());
                    startActivity(intent);
                }
                Log.d("provider user", "email = " + user.getEmail());
                Log.d("provider", "Total = " + stringList.size());
                finish();
            } else {
                Toast.makeText(this, "Some Error be occured", Toast.LENGTH_LONG).show();
                Log.d("provider", "Error = " + repsonse.getError());
            }
        }
    }
}
