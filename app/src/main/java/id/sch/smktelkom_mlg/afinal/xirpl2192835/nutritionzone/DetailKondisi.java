package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Model.DataPasien;

public class DetailKondisi extends AppCompatActivity {

    private TextView tvtb, tvenergi, tvbb, tvprotein, tvkarbohidrat, tvlemak, tvstatus, tvenergimakan;
    private Toolbar mToolbar;
    private String id, nama, status, tee, aktifitas, stress, imt;
    private int bb;
    private int tb;
    private int lemak;
    private int karbohidrat;
    private int protein;
    private int energimakan;
    private int inttee;
    private int tEaten;
    private int kali;
    private Button btnPagi, btnSiang, btnMalam, btnReset;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = mAuth.getCurrentUser();
    private String userId = user.getUid();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailkondisi);
        id = getIntent().getStringExtra("id");
        nama = getIntent().getStringExtra("nama");
        status = getIntent().getStringExtra("status");
        bb = Integer.parseInt(getIntent().getStringExtra("bb"));
        tb = Integer.parseInt(getIntent().getStringExtra("tb"));
        tee = getIntent().getStringExtra("tee");
        aktifitas = getIntent().getStringExtra("aktifitas");
        stress = getIntent().getStringExtra("stress");
        imt = getIntent().getStringExtra("imt");
        tEaten = Integer.parseInt(getIntent().getStringExtra("tEaten"));
        Double dtee = new Double(tee);
        inttee = dtee.intValue();
        lemak = 25 * inttee / 100;
        karbohidrat = 60 * inttee / 100;
        protein = 15 * inttee / 100;

        mToolbar = findViewById(R.id.toolbarKondisi);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(nama);

        tvtb = findViewById(R.id.textViewTB);
        tvbb = findViewById(R.id.textViewBB);
        tvenergi = findViewById(R.id.textViewBEE);
        tvlemak = findViewById(R.id.textViewLemak);
        tvprotein = findViewById(R.id.textViewProtein);
        tvkarbohidrat = findViewById(R.id.textViewKH);
        tvstatus = findViewById(R.id.textViewStatus);
        tvenergimakan = findViewById(R.id.textViewTodayEat);
        btnPagi = findViewById(R.id.buttonPagi);
        btnSiang = findViewById(R.id.buttonSiang);
        btnMalam = findViewById(R.id.buttonMalam);
        btnReset = findViewById(R.id.buttonReset);

        tvtb.setText(tb + " cm");
        tvbb.setText(bb + " kg");
        tvenergi.setText(inttee + " kkal");
        tvprotein.setText(protein + " kkal");
        tvkarbohidrat.setText(karbohidrat + " kkal");
        tvlemak.setText(lemak + " kkal");
        tvstatus.setText(status);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference(userId).child("pasien").child(id).child("check");

        final int teeone = inttee / 3;
        final int teetwo = inttee * 2 / 3;
        final int teethree = inttee;

        if (tEaten == teeone) {
            btnPagi.setEnabled(false);
            kali = 1;
        } else if (tEaten == teetwo) {
            btnPagi.setEnabled(false);
            btnSiang.setEnabled(false);
            kali = 2;
        } else if (tEaten == teethree) {
            btnPagi.setEnabled(false);
            btnSiang.setEnabled(false);
            btnMalam.setEnabled(false);
            kali = 3;
        }
        tvenergimakan.setText(tEaten + " kkal");
        energimakan = tEaten;
        Log.d("valuekali", "kali = " + kali);
        btnPagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnPagi.setEnabled(false);
                ++kali;
                energimakan = inttee * kali / 3;
                tvenergimakan.setText(energimakan + " kkal");
                DataPasien dataPasien = new DataPasien(String.valueOf(tb), String.valueOf(bb), aktifitas, stress, tee, status, imt, String.valueOf(energimakan));
                myRef.setValue(dataPasien);
            }
        });
        btnSiang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSiang.setEnabled(false);
                ++kali;
                energimakan = inttee * kali / 3;
                tvenergimakan.setText(energimakan + " kkal");
                DataPasien dataPasien = new DataPasien(String.valueOf(tb), String.valueOf(bb), aktifitas, stress, tee, status, imt, String.valueOf(energimakan));
                myRef.setValue(dataPasien);

            }
        });
        btnMalam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMalam.setEnabled(false);
                ++kali;
                energimakan = inttee * kali / 3;
                tvenergimakan.setText(energimakan + " kkal");
                DataPasien dataPasien = new DataPasien(String.valueOf(tb), String.valueOf(bb), aktifitas, stress, tee, status, imt, String.valueOf(energimakan));
                myRef.setValue(dataPasien);
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnPagi.setEnabled(true);
                btnSiang.setEnabled(true);
                btnMalam.setEnabled(true);
                energimakan = 0;
                kali = 0;
                tvenergimakan.setText(energimakan + " kkal");
                DataPasien dataPasien = new DataPasien(String.valueOf(tb), String.valueOf(bb), aktifitas, stress, tee, status, imt, String.valueOf(energimakan));
                myRef.setValue(dataPasien);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
