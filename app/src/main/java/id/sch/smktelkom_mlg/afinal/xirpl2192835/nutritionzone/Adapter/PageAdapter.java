package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Fragment.BiodataFragment;
import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Fragment.GiziFragment;
import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Fragment.KondisiFragment;

public class PageAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new BiodataFragment();
            case 1:
                return new KondisiFragment();
            case 2:
                return new GiziFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
