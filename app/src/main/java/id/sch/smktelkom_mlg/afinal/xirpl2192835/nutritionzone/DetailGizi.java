package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class DetailGizi extends AppCompatActivity {

    private String nama, tee, id;
    private int inttee, lemak, karbohidrat, protein, makan3x;
    private Toolbar mToolbar;
    private TextView teep, tees, teem, lemakp, lemaks, lemakm, proteinp, proteins, proteinm, khp, khs, khm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gizi);

        id = getIntent().getStringExtra("id");
        nama = getIntent().getStringExtra("nama");
        tee = getIntent().getStringExtra("tee");
        Double dtee = new Double(tee);
        inttee = dtee.intValue();
        makan3x = inttee / 3;
        lemak = 25 * makan3x / 100;
        karbohidrat = 60 * makan3x / 100;
        protein = 15 * makan3x / 100;

        mToolbar = findViewById(R.id.toolbarGizi);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(nama);

        teep = findViewById(R.id.textViewTEEPagi);
        tees = findViewById(R.id.textViewTEESiang);
        teem = findViewById(R.id.textViewTEEMalam);
        lemakp = findViewById(R.id.textViewLemakPagi);
        lemaks = findViewById(R.id.textViewLemakSiang);
        lemakm = findViewById(R.id.textViewLemakMalam);
        proteinp = findViewById(R.id.textViewProteinPagi);
        proteins = findViewById(R.id.textViewProteinSiang);
        proteinm = findViewById(R.id.textViewProteinMalam);
        khp = findViewById(R.id.textViewKHPagi);
        khs = findViewById(R.id.textViewKHSiang);
        khm = findViewById(R.id.textViewKHMalam);

        teep.setText(makan3x + " kkal");
        lemakp.setText(lemak + " kkal");
        proteinp.setText(protein + " kkal");
        khp.setText(karbohidrat + " kkal");
        tees.setText(makan3x + " kkal");
        lemaks.setText(lemak + " kkal");
        proteins.setText(protein + " kkal");
        khs.setText(karbohidrat + " kkal");
        teem.setText(makan3x + " kkal");
        lemakm.setText(lemak + " kkal");
        proteinm.setText(protein + " kkal");
        khm.setText(karbohidrat + " kkal");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
