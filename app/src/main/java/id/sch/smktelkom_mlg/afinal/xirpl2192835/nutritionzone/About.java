package id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Fragment.HelmiFragment;
import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Fragment.NikenFragment;
import id.sch.smktelkom_mlg.afinal.xirpl2192835.nutritionzone.Fragment.YogaFragment;

public class About extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mToolbar = findViewById(R.id.toolbarAbout);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        bottomNavigationView = findViewById(R.id.bottom_navigationnn);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.Helmi:
                                setFragment(new HelmiFragment());
                                return true;
                            case R.id.Niken:
                                setFragment(new NikenFragment());
                                return true;
                            case R.id.Yoga:
                                setFragment(new YogaFragment());
                                return true;
                            default:
                                return false;
                        }
                    }
                });
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutAbout, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setFragment(new HelmiFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
